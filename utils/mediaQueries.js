import styles from '@/assets/css/01-settings/_settings.breakpoints.scss'

const mq = {
  isPhablet: () => { return window.matchMedia( "(min-width: " + styles.bkpPhablet + ")" ).matches },
  isTablet: () => { return window.matchMedia( "(min-width: " + styles.bkpTablet + ")" ).matches },
  isDesktop: () => { return window.matchMedia( "(min-width: " + styles.bkpDesktop + ")" ).matches },
  isWide: () => { return  window.matchMedia( "(min-width: " + styles.bkpWide + ")").matches },
  isHD: () => { return window.matchMedia( "(min-width: " + styles.bkpHD + ")" ).matches }
};

export default mq
