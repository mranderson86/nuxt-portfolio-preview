import portfolioData from '@/assets/js/portfolioData'

const namespaced = true

export const state = () => ({
  portfolioData: portfolioData,
  initialArea: null,
  siteTitle: 'Lee Anderson',
  siteSubtitle: 'Designer+Developer',
  contactEmail: 'mranderson.creative@gmail.com',
  bitbucketRepoLink: 'https://bitbucket.org/mranderson86/nuxt-portfolio-preview-for-bridgeu/src/master/',
  contactSubject: 'Hey Lee! Let\'s make something awesome together.',
  siteLoaded: false,
  pending: false,
  isAnimating: true,
  isMobileDevice: null,
  activeMenu: 'PortfolioProjects',
  activeSwiperSlideIndex: 0,
  config: {
    showBottomFade: false,
    showBorderTriangles: false,
    showChatScreen: false,
    showLogo: false,
    showMainNavbar: false,
    showPortfolio: false,
    showPortfolioNavbar: false,
    showWelcomeScreen: false
  }
})

export const getters = {
  getContactHref(state) {
    return `mailto:${state.contactEmail}?subject=${state.contactSubject}`
  }
}

export const actions = {
  setInitialArea({commit}, initialArea) {
    commit('setInitialArea', initialArea)
  },

  setIsMobileDevice({commit}, isMobileDevice) {
    commit('setIsMobileDevice', isMobileDevice)
  },

  siteLoaded({commit, dispatch, state}) {
    commit('setSiteLoaded', true)
  },

  pendingStart({commit}) {
    commit('setPending', true)
  },

  pendingStop({commit}) {
    commit('setPending', false)
  },

  animationStarted({commit}) {
    commit('setIsAnimating', true)
  },
  animationComplete({commit}) {
    commit('setIsAnimating', false)
  },

  homepageEnter({dispatch, state}) {
    dispatch('showBottomFade')
    dispatch('showPortfolioNavbar')
    dispatch('showChatScreen')

    if (!state.config.showPortfolio) {
      dispatch('showMainNavbar')
    }
  },

  homepageLeave({dispatch}) {
    dispatch('hidePortfolioNavbar')
    dispatch('hideMainNavbar')
    dispatch('hideLogo')
    dispatch('showPortfolio')
  },

  projectEnter({dispatch}) {
    console.log('project entered')
    dispatch('showPortfolio')
  },


  showMainNavbar({commit}) {
    commit('setShowMainNavbar', true)
  },
  hideMainNavbar({commit}) {
    commit('setShowMainNavbar', false)
  },


  showLogo({commit}) {
    commit('setShowLogo', true)
  },
  hideLogo({commit}) {
    commit('setShowLogo', false)
  },

  async togglePortfolio({commit, dispatch, state}) {
    if (state.isAnimating) return
    dispatch('animationStarted')

    if (state.config.showPortfolio) {
      dispatch('hidePortfolio')
    } else {
      dispatch('showChatScreen')
      dispatch('showPortfolio')
    }
  },

  hidePortfolio({commit, dispatch}) {
    dispatch('showBorderTriangles')
    dispatch('showMainNavbar')
    commit('setShowPortfolio', false)
  },

  showPortfolio({commit, dispatch}) {
    dispatch('hideBorderTriangles')
    dispatch('hideMainNavbar')
    commit('setShowPortfolio', true)
  },

  showPortfolioNavbar({commit, state}) {
    commit('setShowPortfolioNavbar', true)
  },
  hidePortfolioNavbar({commit, state}) {
    commit('setShowPortfolioNavbar', false)
  },

  showWelcomeScreen({commit}) {
    commit('setShowWelcomeScreen', true)
  },
  hideWelcomeScreen({commit}) {
    commit('setShowWelcomeScreen', false)
  },

  showChatScreen({commit}) {
    commit('setShowChatScreen', true)
  },

  setActiveMenu({commit, dispatch}, menu) {
    commit('setActiveMenu', menu)
  },

  showBorderTriangles({commit}) {
    commit('setShowBorderTriangles', true)
  },
  hideBorderTriangles({commit}) {
    commit('setShowBorderTriangles', false)
  },

  showBottomFade({commit}) {
    commit('setShowBottomFade', true)
  },
  hideBottomFade({commit}) {
    commit('setShowBottomFade', false)
  },

  setActiveSwiperSlideIndex({commit}, index) {
    commit('setActiveSwiperSlideIndex', index)
  }
}

export const mutations = {
  setInitialArea(state, initialArea) {
    state.initialArea = initialArea
  },
  setSiteLoaded(state, bool) {
    state.siteLoaded = bool
  },
  setIsMobileDevice(state, bool) {
    state.isMobileDevice = bool
  },
  setPending(state, bool) {
    state.pending = bool
  },
  setShowMainNavbar(state, bool) {
    state.config.showMainNavbar = bool
  },
  setShowLogo(state, bool) {
    state.config.showLogo = bool
  },
  setShowPortfolio(state, bool) {
    state.config.showPortfolio = bool
  },
  setShowPortfolioNavbar(state, bool) {
    state.config.showPortfolioNavbar = bool
  },
  setShowBorderTriangles(state, bool) {
    state.config.showBorderTriangles = bool
  },
  setShowBottomFade(state, bool) {
    state.config.showBottomFade = bool
  },
  setShowWelcomeScreen(state, bool) {
    state.config.showWelcomeScreen = bool
  },
  setShowChatScreen(state, bool) {
    state.config.showChatScreen = bool
  },
  setIsAnimating(state, bool) {
    state.isAnimating = bool
  },
  setActiveMenu(state, menu) {
    state.activeMenu = menu
  },
  setActiveSwiperSlideIndex(state, index) {
    state.activeSwiperSlideIndex = index
  }
}
