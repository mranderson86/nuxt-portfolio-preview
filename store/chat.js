import { dialogue } from '@/assets/js/dialogue.js'

const namespaced = true

export const state = () => ({
  isMuted: true,
  dialogue: dialogue,
  activeIndex: null
})

export const getters = {
  activeDialogue: state => {
    return state.activeIndex >= 0 ? state.dialogue[state.activeIndex] : null
  }
}

export const actions = {
  getDialogue({ commit, state }, label) {
    let index = state.dialogue.findIndex(item => item.label === label)
    commit('setActiveIndex', index)
  },
  muteDialouge({ commit }) {
    commit('setIsMuted', true)
  },
  unmuteDialogue({ commit }) {
    commit('setIsMuted', false)
  }
}

export const mutations = {
  setIsMuted(state, bool) {
    state.isMuted = bool
  },
  setActiveIndex(state, index) {
    state.activeIndex = index
  }
}
