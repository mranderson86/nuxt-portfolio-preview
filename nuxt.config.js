
export default {
  ssr: false,

  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico?v=1' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Montserrat' }
    ]
  },

  loading: {
    color: '#d03a69',
    height: '4px',
    throttle: 0
  },

  css: [
    '@/assets/css/style.scss'
  ],

  plugins: [],

  buildModules: [],

  modules: [
    'nuxt-rfg-icon',
    '@nuxtjs/axios',
    'nuxt-responsive-loader',
    '@nuxtjs/style-resources',
    ['nuxt-lazy-load', { directiveOnly: true }]
  ],

  styleResources: {
    scss: [
      './assets/css/01-settings/*.scss',
      './assets/css/02-tools/*.scss'
    ]
  },

  responsiveLoader: {
    name: 'img/[hash:7]-[width].[ext]',
    min: 640,
    max: 1168,
    steps: 5,
    placeholder: true,
    quality: 100
  },

  axios: {},

  build: {
    extend (config, ctx) {}
  }
}
