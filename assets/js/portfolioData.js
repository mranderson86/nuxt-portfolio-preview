
//==============================================================================
// 1: Define Portfolio Data
//==============================================================================
const menuLinkData = [
  ['Projects'],
  ['Code'],
  ['Design']
]

// [title, subtible, [tags], theme]
const projectData = [
  ['PlayDJ', 'The Home of DJ Livestreaming', ['UX/UI', 'Design', 'Front-End Dev', 'Vue'], 'strawberry'],
  ['Wanderwall', 'Photo Pinmaps for Travel Lovers', ['UX/UI', 'Design', 'Front-End Dev', 'Vue', 'Firebase'], 'ocean'],
  ['Leading Edge', 'Height Safety. Made Simple.', ['Design', 'Front-End Dev', 'Copywriting', 'Craft CMS'], 'nectarine'],
  // ['Smelly Fruit', 'Stink Less At Thai', ['Design', 'Full-Stack Dev'], 'banana'],
  // ['Feel Think Do', 'A Guided Daily Journal', ['Design', 'Front-End Dev', 'Copywriting', 'WordPress'], 'sweetpotato'],
  // ['Heighthound', 'Find the best Work at Height training near you.', ['Design', 'Front-End Dev', 'Copywriting', 'Craft CMS'], 'sweetpotato'],
  // ['Coconut', 'Step Sequenced Mbiras', [],'']
]

// [title, slug, urlCode]
const codepenData = [
  ['Product Landing Page', 'landing-page', 'VKKrGB'],
  ['Grid/List View Switcher', 'grid-list-view', 'KNNdLE'],
  ['Progressive Disclosure Table','progressive-disclosure-table','XpQRdY'],
  ['Icon List Boxes','icon-list-boxes','ZeLBvb'],
  ['Logo Reveal', 'css-logo-reveal', 'yVvzVM'],
  ['Retro Squares', 'retro-squares', 'pRRXgG'],
  ['Coloured Buttons', 'sassy-buttons','LxpjXz'],
  ['Doughnut Chart','doughnut-chart','QdKZjr'],
  // ['Experimental Pop Menu','grid-list-view',''],
]

// [title, alt, description, filename]
const galleryData = [
  ['Leading Edge', '', '', 'le-mockups'],
  ['Fuel Proof', '', '', 'fuel-proof'],
  ['Height Safety Brochure', '', '', 'height-brochure'],
  ['Rio Earsaver', '', '', 'rio'],
  ['Rio Earsaver', '', '', 'rio-earsaver'],
  ['Butterly', '', '', 'butterfly'],
  ['Great White', '', '', 'great-white'],
  ['Cobra', '', '', 'cobra'],
  ['Jackal Albums', '', '', 'jackal-covers'],
  ['LE Catalogue', '', '', 'le-minimal-catalogue'],
  ['Altitude Catalogue', '', '', 'altitude'],
  ['Cirkt', '', '', 'cirkt'],
  ['Portrait', '', '', 'ink-portrait'],
  ['Danny', '', '', 'danny-portrait'],
  ['Book', '', '', 'book'],
  ['Height Safety Icons', '', '', 'height-safety-icons'],
  ['Sunny', '', '', 'sunny'],
  ['Grass Tiger', '', '', 'grass-tiger'],
  // 'latex-montage',
  // 'nina-kate',
  // 'altitude-inside',
  // 'hood',
  // 'sam',
  // 'kitchen',
  // 'camino',
  // 'joe-mockup',
]


//==============================================================================
// 2: Create Object Factories
//==============================================================================
const renderPortfolioData = (items, renderer) => {
  const data = []

  items.forEach(item => {
    const object = renderer(...item)
    data.push(object)
  })

  return data
}

const menuLinkFactory = (label) => {
  const slug = `Portfolio${label}`
  return {
    label,
    slug
  }
}

const projectFactory = (title, subtitle, tags, theme) => {
  const slug = title.toLowerCase().replace(/\s/g, '')
  const imgObject = require(`~/assets/img/project-backgrounds/bg-${slug}.jpg`)
  const { placeholder, srcSet } = imgObject

  return {
    title,
    slug,
    subtitle,
    tags,
    theme,
    placeholder,
    srcSet
  }
}

const galleryFactory = (title, alt, description, imgSrc) => {
  const imgObj = require(`~/assets/img/${imgSrc}.jpg`)
  const { placeholder, srcSet } = imgObj
  const src = imgObj.images[imgObj.images.length - 1].path

  return {
    title,
    alt,
    description,
    placeholder,
    srcSet,
    src
  }
}

const codepenFactory = (title, imgSrc, codepen) => {
  const prefix = 'https://codepen.io/mranderson86'
  const imgObject = require(`~/assets/img/${imgSrc}.jpg`)
  const { placeholder, srcSet } = imgObject
  const urlFull = `${prefix}/full/${codepen}`
  const urlPen = `${prefix}/pen/${codepen}`

  return {
    title,
    placeholder,
    srcSet,
    urlFull,
    urlPen
  }
}


//==============================================================================
// 3: Render The Portfolio
//==============================================================================
const menuLinks = renderPortfolioData(menuLinkData, menuLinkFactory)
const projects  = renderPortfolioData(projectData, projectFactory)
const design    = renderPortfolioData(galleryData, galleryFactory)
const code      = renderPortfolioData(codepenData, codepenFactory)

const portfolioData = {
  menuLinks,
  projects,
  design,
  code
}

export default portfolioData
