import { gsap } from 'gsap'

export function parallaxIt(e, target, movement, containerClass) {
  const container = document.querySelector(containerClass)
  let rect = target.getBoundingClientRect()
  let offset = {
    top: rect.top + window.scrollY,
    left: rect.left + window.scrollX,
  }
  let relX = e.pageX - offset.left
  let relY = e.pageY - offset.top
  gsap.to(target, 1, {
    rotationY: (relX - container.offsetWidth/2) / container.offsetWidth * movement,
    rotationX: (relY - container.offsetHeight/2) / container.offsetHeight * -movement,
    ease: "Power3.easeOut"
  })
}
