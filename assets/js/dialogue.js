export const dialogue = [
  {
    label: "softWelcome",
    m: [
      { text: 'Sure!', duration: 1000, audio: '' },
      { text: 'How can I help?', duration: 1000, audio: '' }
    ],
    q: [
      { text: "How can you help me?", next: "help" },
      { text: "What you doin' up here?", next: "stratosphere" },
    ]
  },
  {
    label: "welcome",
    m: [
      { text: "Greetings traveller! I'm Lee.", duration: 1200, audio: '' },
      { text: "Designer+Developer from the UK.", duration: 1500, audio: '' },
      { text: "Helping small-business and startups craft online experiences.", duration: 1500, audio: '' }
    ],
    q: [
      { text: "What can you do for me?", next: "help" },
      { text: "Why are you up here?", next: "stratosphere" },
    ]
  },
  {
    label: "help",
    m: [
      { text: 'Bridging the gap between design and development means I can:', duration: 1300, audio: '' },
      { text: '1) Craft wireframes, mockups and high-res visual designs.', duration: 1700, audio: '' },
      { text: '2) Bring them to life with HTML, CSS and JavaScript.', duration: 1900, audio: '' },
      { text: '3) Code fully interactive data-driven prototypes in frameworks like Vue.js.', duration: 1900, audio: '' },
    ],
    q: [
      { text: "Why do I need that?", next: "why"},
      { text: "Let me ask something else...", next: "softWelcome" },
    ]
  },
  {
    label: "stratosphere",
    m: [
      { text: "Came up for the view.", duration: 1500, audio: ''},
      { text: "It's nice to see the bigger picture...", duration: 1300, audio: ''},
      { text: "...how all the pieces fit together.", duration: 1300, audio: ''},

    ],
    q: [
      { text: "What's your story?", next: "story" },
      { text: "Let me ask something else...", next: "softWelcome" },
    ]
  },
  {
    label: "why",
    m: [
      { text: 'By bringing a mixture of critical thinking, creativity and technical proficiency...', duration: 2000, audio: ''},
      { text: '...I make planning, designing and developing web projects easier for you and your teams.', duration: 1500, audio: ''},
    ],
    q: [
      { text: "What are you doing up here?", next: "stratosphere" },
      { text: "Let me ask something else...", next: "softWelcome" },
    ]
  },
  {
    label: "story",
    m: [
      { text: "I was always into Art, Music and Technology.", duration: 1500, audio: '' },
      { text: "Grew up playing in bands that needed websites, graphics and merch.", duration: 2300, audio: '' },
      { text: "So, I taught myself how to design and code.", duration: 2300, audio: '' },
      { text: "Then I kept on learning!", duration: 1000, audio: '' }
    ],
    q: [{ text: "Let me ask something else...", next: "softWelcome" }]
  }
]
