import { gsap, Sine } from 'gsap'

export class Float {
  constructor(el) {
    this.el = el
    this.play = true;
    this.randomX = this.random(5, 10);
    this.randomY = this.random(7, 14);
    this.randomDelay = this.random(2, 5);
    this.randomTime = this.random(4, 5);
    this.randomTime2 = this.random(5, 8);
    this.randomAngle = this.random(0, 2);
  }

  init() {
    console.log('started!')
    this.play = true
    this.moveX(this.el, 1)
    this.moveY(this.el, -1)
    this.rotate(this.el, 1)
  }

  halt() {
    this.play = false
  }

  loop(method, target, direction) {
    if (!this.play) return
    switch (method) {
      case 'rotate':
        this.rotate(target, direction)
        break;
      case 'moveX':
        this.moveX(target, direction)
        break;
      case 'moveY':
        this.moveY(target, direction)
        break;
    }
  }

  rotate(target, direction) {
    if (!this.play) return
    gsap.to(target, this.randomTime2(), {
      rotation: this.randomAngle(direction),
      ease: Sine.easeInOut,
      onComplete: () => { this.loop('rotate', target, direction * -1) }
    })
  }

  moveX(target, direction) {
    gsap.to(target, this.randomTime(), {
      x: this.randomX(direction),
      ease: Sine.easeInOut,
      onComplete: () => { this.loop('moveX', target, direction * -1) }
    })
  }

  moveY(target, direction) {
    gsap.to(target, this.randomTime(), {
      y: this.randomY(direction),
      ease: Sine.easeInOut,
      onComplete: () => { this.loop('moveY', target, direction * -1) }
    })
  }

  random(min, max) {
    const delta = max - min;
    return (direction = 1) => (min + delta * Math.random()) * direction;
  }

  reset() {
    this.play = false
    gsap.to(this.el, 1, {
      rotation: 0,
      x: 0,
      y: 0
    })
  }
}
